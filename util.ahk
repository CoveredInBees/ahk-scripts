#include %A_ScriptDir%\com.ahk
;#include %A_ScriptDir%\acc.ahk
#include C:\data\autohotkey\locationdependent_defs.ahk

rssdir := 0
;return

SaveAsTrick()
{
	clipsave := clipBoardAll
	
	SendInput !fa
	Clipboard :=
	Sleep 500
	SendInput ^c
	ClipWait 1
	filename := Clipboard

	sleep 51
	clipboard := clipsave
	
	send {esc}
	return filename
}

copyFullPath()
{
	;if WinActive("ahk_class dopus.lister"); or WinActive("Save As")
	;{
	;}
	;else
	if WinActive("Notepad++") or WinActive("ahk_class dopus.lister") or winactive("Sublime Text 2")
	{
		clipsave := clipBoardAll
		clipboard :=
		
		;sleep 50
		
		;WinMenuSelectItem, , , Edit, Copy to Clipboard, Current Full File path to Clipboard
		Send +^c
		ClipWait 1
		path := clipboard

		sleep 51
		clipboard := clipsave
		return path
	}
	else if WinActive("Microsoft Visual Studio") 
	{
		clipsave := clipBoardAll4
		clipboard :=
		
		Send +^c ; bound in visual studio as File.CopyFullPath
		sleep 100

		ClipWait 1
		path := clipboard
		sleep 51
		clipboard := clipsave
		;msgbox % path
		return path
	}
	else if WinActive("Beyond Compare")
	{
		Sleep 200
		filename := SaveAsTrick()
		return
		SendInput +{tab}%filename%
		path := GetSelection()
		SendInput {esc}
		return path
	}	
	else if WinActive("ahk_class CabinetWClass")
		return GetSelection()
	else
		return saveAsTrick()
}

GetSelection(convertSpaces = 0)
{
	clipsave := clipBoardAll
	clipboard :=

	Send, ^c
	ClipWait 1
	path := Clipboard
	if (convertSpaces)
		path := strreplace(path, a_space, "%20")
	 
	sleep 51
	clipboard := clipsave
	Return path
}

PutText3(MyText)
{
	Send {Raw}%MyText%
}

PutText2(MyText)
{
	clipsave := clipboardall

	clipboard := MyText
	Sleep 75 ; down from 100

	ifwinactive ~
		Send +{insert}
	else ifwinactive Microsoft Excel
	{
		Send ^v
		ifwinactive Microsoft Excel
			sleep 500
	}
	else
		Send ^v

	Sleep 100 ; up from 20

	sleep 51
	clipboard := clipsave
}

perforceLocate_internal(file, diff)
{
	perforce()
		
	send ^0 ; depot tree to fix find in files location
	sleep 300
	send {pgup 3} ; highlight root
	sleep 300
	Send +^f
	sleep 100
	puttext3(file)
	Sleep 100
	Send {return}
	return
	if (diff)
	{
		Sleep 1000
		Send ^d
	}
}

perforceLocate(file)
{
	perforceLocate_internal(file, 0)
}

perforceDiff(file)
{
	perforceLocate_internal(file, 1)
}

perforceOpenForEdit(file)
{
	global MyProgramFiles
	
	str := MyProgramFiles . "\Perforce\p4.exe edit " . file
	Run % str
}

AddSeparator()
{
	if winactive("flynn.txt")
		PutText2("-- checkin`n")
	else if winactive("rhizome.txt")
		PutText2("-- checkin`n")
	else if winactive("home.txt")
		PutText2("----------`n")
	else if winactive(".txt")
		PutText2("------------------------`n")
	else if winactive("Slack - ")
	{
		keywait alt
		PutText2("*Checkin:* ")
	}
	else
		PutText2("//-------------------------------------------------------------------------------------------------`n")
}

Explore3(path2)
{
	global MyExplorer
	global MyExplorerDir
	;cmd := MyExplorer . " """ . path2 . """"
	cmd := MyExplorer . " " . path2
	run % cmd, % MyExplorerDir
	winwaitactive ,,, 2
}

Explore2(path2)
{
	global MyExplorer
	ifwinexist ahk_class ATL:ExplorerFrame
	{
		winactivate
		WinMenuSelectItem, , , Tools, Run command
		sleep 50
		Send +{home}
		puttext3(path2)
		sleep 50
		Sendinput {return}
	}
	else
	{
		cmd := MyExplorer . " /1 " . """" . path2 . """"
		run % cmd
		winwaitactive ,,, 2
	}
}

Truecrypt(tcpath, expath)
{
	if (!FileExist(exPath))
	{
		global MyProgramFiles
		RunWait % "c:\program files\truecrypt\TrueCrypt.exe /v " . tcpath . " /l o /q /m rm"
		;RunWait % "C:\Program Files\VeraCrypt\VeraCrypt.exe /v " . tcpath . " /l o /q"
		;Sleep 500
		;WinWaitClose Enter password for
		Sleep 500
	}
	Explore(expath)
}

focus_url_bar()
{
	send ^l
}

Browser(swap := 0)
{
	global BrowserExe
	global lastId
	IfWinExist, ahk_exe %BrowserExe%
	{
		WinGet, fensterID, List, ahk_exe %BrowserExe%
		bestCoord := -999999
		bestId := -1
		Loop, %fensterID% 
		{
			id := fensterID%A_Index%
			wingettitle title, ahk_id %id%
			if (instr(title, "slack - "))
				continue
			if (instr(title, "Chrome Remote Desktop"))
				continue
			WinGetPos, x, y, w, h, ahk_id %id%
			;msgbox % "found " . x . ", " . y . ": " . title
			;if ((x > bestCoord && x < 1920) || x < -10000 ) ; sometimes fullscreen windows on primary display have x = -32000
			if (swap == 1)
			{
				winGet, ismin, MinMax, ahk_id %id%
				;msgbox % title . " " . ismin
				if (ismin == -1)
					continue
				
				if (bestId == -1 || id != lastId)
					bestId := id
			}
			else
			{
				winGet, ismin, MinMax, ahk_id %id%
				if (ismin == -1)
					y -= 22222
				; if ((y > bestCoord) || y < -10000 ) ; sometimes fullscreen windows on primary display have x = -32000
				if (y > bestCoord)
				{
					;bestCoord := x
					;msgbox % "best y = " . y . ", title = " . title
					bestCoord := y
					bestId := id
				}
			}
		}
		if (bestId != -1)
		{
			;msgbox % "chose " . bestCoord
			WinActivate, ahk_id %bestId%
			lastId := bestId
			sleep 100
		}

		;WinActivate
		;WinWaitActive ,,, 2
		;Send, ^t
	}
	else
	{
		global BrowserLocation
		Run %BrowserLocation%
		winwaitactive ahk_exe %BrowserExe%
	}

	;focus_url_bar()
	return
}

Perforce()
{
	ifwinexist Perforce
		winactivate Perforce
	else
	{
		run C:\Program Files\Perforce\p4v.exe
	}

	winwaitactive Perforce
}

ExploreXY(path)
{
	cmd := "C:\Program Files (x86)\XYplorer\XYplorer.exe """ . path . """"
	run % cmd
}

Explore(path)
{
	;run c:\program files\gpsoftware\directory opus\dopusrt.exe /cmd go c:\data
	;return
	cmd := "c:\program files\gpsoftware\directory opus\dopusrt.exe /cmd go """ . path . """"
	run % cmd
	;msgbox % cmd
	cmd := "c:\program files\gpsoftware\directory opus\dopusrt.exe /cmd go LASTACTIVELISTER"
	run % cmd
	;sleep 100
	;run % cmd
	;sleep 100
	;run % cmd
	;winactivate ahk_class dopus.lister
}

min2Tray(name)
{
	WinGet, id, ID, %name%
	global myahk
	str := MyAHK . "/AutoHotkey.exe C:\data\Dropbox\autohotkey\Min2Tray.ahk " . id
	;msgbox % str
	run % str
}

spyder(console)
{
	ifwinexist Spyder ahk_class QWidget
	{
		winactivate Spyder ahk_class QWidget
		if (console)
			send !2
		return
	}

	global PythonDir
	pw := PythonDir . "\pythonw.exe"

	str := pw . " C:\Anaconda3\cwp.py C:\Anaconda3 ""C:/Anaconda3/pythonw.exe"" ""C:/Anaconda3/Scripts/spyder-script.py"""
	run % str
	winwaitactive Spyder ahk_class QWidget
	sleep 200
	if (console)
		send !2

	;send !i{down 2}{return}
}

ipython()
{

	ifwinexist Jupyter QtConsole
		winactivate Jupyter QtConsole
	else
	{
		run C:\Anaconda3\pythonw.exe C:\Anaconda3\cwp.py C:\Anaconda3 "C:/Anaconda3/pythonw.exe" "C:/Anaconda3/Scripts/jupyter-qtconsole-script.py"
		while (!winexist("Jupyter QtConsole"))
		{

			sleep 200
		}
		winactivate Jupyter QtConsole
	}
}

ipython3()
{
	ifwinexist IPython: 
		winactivate IPython: 
	else
		run C:\Anaconda3\python.exe C:\Anaconda3\cwp.py C:\Anaconda3 "C:/Anaconda3/python.exe" "C:/Anaconda3/Scripts/ipython-script.py"
}

ipython2()
{
	ifwinexist IPython ahk_class QWidget
	{
		winactivate IPython ahk_class QWidget
		return
	}

	global PythonDir
	ippath := PythonDir . "\scripts\ipython.exe"
	Run % ippath . " qtconsole --colors LightBG --ConsoleWidget.font_size=10 --no-confirm-exit", c:\data\dropbox\python
	;Run % ippath . " qtconsole --colors linux --ConsoleWidget.font_size=10 --no-confirm-exit", c:\data\dropbox\python
	winwait % ippath . " ahk_class ConsoleWindowClass"
	; c:\data\python27\scripts\ipython.exe ahk_class ConsoleWindowClass

	;winwait Ipython ahk_class ConsoleWindowClass
	sleep 100
	min2Tray(ippath . " ahk_class ConsoleWindowClass")
	winwait IPython ahk_class QWidget
	sleep 100
	loop 40
	{
		ifwinactive IPython ahk_class QWidget
		{
			sleep 250
			Send #{left}
			return
		}
		winactivate IPython ahk_class QWidget
		sleep 250
	}

	winwaitactive IPython ahk_class QWidget
	sleep 100
	;winmaximize A
	Send #{numpad6}
}

startPidgin()
{
	global MyProgramFiles
	Run % MyProgramFiles . "\Pidgin\pidgin.exe"
}

doesProcessExist(proc)
{
	Process, wait, %proc%, 0.5
	 
	NewPID = %ErrorLevel%
	if NewPID = 0
		return 0
	Else
		return 1
}

insertRow()
{
	Send +{space}
	send ^{numpadadd}
}

deleteRow()
{
	Send +{space}
	send ^{numpadsub}
}

outlookCopyEmailTitle()
{
	msg := copyFullPath()
	StringTrimRight, msg, msg, 4
	msg := "@""" . msg . """"
	clipboard := msg
}

Chr2(UnicodeCode)
{
	VarSetCapacity(TempVar,2), NumPut(UnicodeCode,TempVar,0,"UShort")
	Return, TempVar
}

edit(file)
{
	global MyEditorSublimeHack
	if (MyEditorSublimeHack)
	{
		winactivate Sublime Text 2
		keywait alt
	}
	
	global MyEditor
	comm := MyEditor . " " . """" . file . """"
	;msgbox % comm
	run % comm
	if (MyEditorSublimeHack)
	{
		winwait Sublime Text 2
		sleep 100
		winactivate Sublime Text 2
		send {esc}
		sleep 200
		winactivate Sublime Text 2
	}
}

youtube_fullscreen()
{
	controlclick , x450 y360, ahk_class MozillaWindowClass
	;send +!f ; stop music
	;mouseclick left, x450 y360, ahk_class MozillaWindowClass
	sleep 750
	send f
	sleep 750
	controlclick , x450 y360, ahk_class ShockwaveFlashFullScreen
}

MyDateWee()
{
	;FormatTime, MyDate,, dd MMM yyyy
	FormatTime, MyDate,, yyyy-MM-dd
	;ifwinactive Google Docs
	;	Send ^b%MyDate%^b{Enter}{Enter 1}{up 1}
	;else 
	ifwinactive Microsoft Visual Studio
		puttext3(MyDate)
	else ifwinactive Microsoft Excel
		puttext3(MyDate)
	else
		puttext2(MyDate)
}

Bracket(start, end)
{
	clipsave := clipBoardAll
	clipboard :=

	;Sleep 50
	SendInput ^x
	ClipWait, 0.5

	string := Clipboard
	strings := strsplit(string, "`n")
	str2 := ""
	for k, v in strings
	{
		if (k >= 2)
			str2 := str2 . "`n"
	    str1 := v
	    ;msgbox % "[" . str1 . "]"
	    ;msgbox % SubStr(str1, 1, 1)
		if (SubStr(str1, 1, 1) == "`r")
			StringTrimRight, str1, str1, 1
		pre := ""
		post := ""
		if (SubStr(str1, 1, 1) == " ")
		{
			pre := SubStr(str1, 1, 1)
			str1 := SubStr(str1, 2)
		}
		if (SubStr(str1, 0, 1) == " ")
		{
			post := SubStr(str1, 0, 1)
			str1 := SubStr(str1, 1, -1)
		}
		str2 := str2 . pre . start . str1 . end . post

	}
	ClipBoard := str2
	sleep 50
	Send ^v

	sleep 51
	clipboard := clipsave
}

synergy_change(dir)
{
	if (dir)
		send +^!{f6}
	else
		send +^!{f7}
}

InsertDebugPrint()
{
	global Unity
	if (Unity)
	{
		PutText2("/*cdje*/DevUI.print(""{0}." . method . ": "", name);")
		keywait shift
		Send {left 9}
	}
	else
	{
		PutText2("if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 0, FColor::White, FString(""""));")
		keywait shift
		Send {left 4}
	}
}

InsertPythonPrint()
{
	PutText2("print(f""{}"")")
	keywait shift
	Send {left 3}
}

InsertDebugPrint2()
{
	;send ^b
	;send ^c{esc}+{return}
	;sleep 100
	str := "/*cdje*/DevUI.print(""{0}: = {1}"", name);"
	PutText2(str)
	keywait shift
	keywait control
	Send {Left 14}
	send ^v{space}
	Send {right 12}
	send {,}{space}
	send ^v{esc}
}

VsGetMethodName()
{
	clipsave := clipBoardAll
	clipboard :=

	send X{backspace}
	send +^{pgup}
	send ^c
	ClipWait, 0.2
	str := Clipboard       ; Convert to text
	send ![

	b := instr(str, "(")
	if (b == -1)
		return ""

	s := instr(str, " ", false, b - strlen(str))
	if (s == -1)
		return ""
	s := s + 1

	str := substr(str, s, b-s)
	
	clipboard := clipsave

	return str
}

LogVS2()
{
	;SplitPath, str, name, dir
	;stringleft class, name, StrLen(name) - 4
	;PutText2("/*cdje*/elLog(""" . class . "::\n"");")
	;msgbox % method

	method := VsGetMethodName()
	PutText2("/*cdje*/Debug.LogFormat(this, ""{0}." . method . ":  = {1}"", name, );")
	keywait shift
	keywait control
	Send {Left 17}
	send ^v
	Send {right 15}
	send ^v{esc}
}

ToStringSurround()
{
	clipsave := clipBoardAll
	clipboard :=
	
	Send ^c
	ClipWait, 0.2
	string := Clipboard       ; Convert to text
	stringLen := StrLen(string)
	string := "TOSTR(" . string . ")"

	ClipBoard := string
	SendPlay ^v                       ; For best compatibility: SendPlay
	Sleep 50                      ; Don't change clipboard while it is pasted! (Sleep > 0)

	if (stringLen == 0)
		Send {left 1}
	
	sleep 51
	clipboard := clipsave
}

maybeThrottleStop()
{
	Process, Exist, throttlestop.exe
	pid = %errorLevel%

	If (pid = 0)
		run C:\apps\ThrottleStop\ThrottleStop.exe -> location dependent
}

timed_shutdown()
{
	run "C:\Program Files\Timed Shutdown\Timed Shutdown.exe"
	winwaitactive Timed Shutdown
	send 45
	; sleep 300
	send +{left 2}
}

rss_reader_active()
{
	IfWinActive, Inoreader -
		return true
	IfWinActive, Saved For Later - Mozilla
		return true
	IfWinActive, Saved - Mozilla
		return true

	return false
}

togglerssdir()
{
	global rssdir
	rssdir := 1 - rssdir
	soundbeep 311 * (2 - rssdir), 50
}

browserCloseTab()
{
	browserKey("^{f4}")
}

browserKey(key)
{
	;controlclick , x50 y170
	;msgbox
	;escape_flash()
	send % key
}

escape_flash()
{
	focus_url_bar()
	;sleep 100
	;send {tab 2}
}

myReload()
{
	ifwinactive Sublime Text 2
		send ^s
	Reload
	MsgBox reloaded
}

msgboxtest()
{
	msgbox test
}


