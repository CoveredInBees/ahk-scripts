﻿; +^!{f1} ; dpack find method
; +^!{f2}: sublime
; +^!{f3}: keypirinha
; +^!{f4}: sublime
; +^!{f5}: sublime
; +^!{f6}: ditto paste previous
; +^!{f7}: keepass auto type
; +!{f7}: keepass show window
; +^!{f8}: suspend
; +^!{f9}: reload autohotkey startup.ahk (windows hotkey defined on desktop)
; +^!{f10}: (laptune) activate stylish toolbar button via keyconfig java
; +^!{f11}: (laptune) ditto app

;#Persistent
#KeyHistory 500
;#NoTrayIcon
;#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Event  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory. 
SetTitleMatchMode, 2
 
doneString := "# DONE"
girlfriend := "Verity"
work := 0

#Include c:\data\autohotkey\locationdependent.ahk
;#Include %A_ScriptDir%\util.ahk
#include %A_ScriptDir%\trayicon.ahk

#IfWinActive paint.net
	$^!v::send ^!v
	;9::send $^!v
#ifwinactive

#IfWinActive ahk_exe AIMP.exe
	^r::send +U
#ifwinactive

#IfWinActive ahk_class TAIMPMainForm
	f2::send +^!{f6}
#ifwinactive

#IfWinActive ahk_exe Skype.exe ahk_class tSkMainForm
	!f4::WinClose
#IfWinActive

#IfWinActive Google Sheets
	^m::gsmerge()
#IfWinActive

#IfWinActive Screenshot Captor
	^c::Send !ecc
#IfWinActive

#IfWinActive slither.io
	^q::Send WORM-CEPTION{!}{return}
#IfWinActive

#IfWinActive Plastic SCM
	!up::send ^n
	!down::send ^m
	^w::send ^n
	^e::send ^m
#IfWinActive

#IfWinActive Visual Studio Code
	+!p::vscprint()
	^+!w::vscAddToWatch()
#IfWinActive

#IfWinActive Microsoft Visual Studio
	+^space::send {space}{left}^{space}
	^0::PutText2("{0}")
	^1::PutText2("{1}")
	^2::PutText2("{2}")
	;#1::saveVSAndSwitchToUnity()
	;#2::saveAndRestartUnity()
	#1::switchToUnreal()
	;#2::UeCompileRun()
	^3::VectorText()
	;+^4::perforceLocateFromVS()
	;f6::commonCppFile()
	;f6::saveAndRestartUnity()
	;^f6::commonCppFile()
	^F8::debugAndRestartUnity()
	$f12::FindRefsGotoDef()
	;f8::restartDebugging()
	+^[::InsertCurlyBraces()
	;+!=::PutText2(" != null") ; key used for music volume
	^p::PutText2("transform.position")
	+^p::send ^q
	^q::PutText2("/*cdje*/")
	!q::ToggleCdje(2)
	+!q::FindCdje()
	+^!q::stampcdje()
	+^q::DocumentPublicVars()
	+^y::IfNonNull()
	;!e::send !c ; // for case toggle in vs find dialog
	^u::stopUnityFromOtherApp()
	;+^e::browseToCurrentFile()
	;^t::TuneSurround()
	^t::PutText2("Time.deltaTime")
	;+!m::send +^!{f1} ; dpack find method
	+^m::PutText2("Mathf.")
	;+!m::NewMethod()
	;^m::send {ctrl up}!m
	+^n::PutText2("[System.NonSerialized] public ")
	+^!n::formerly()
	+^t::Tooltip()
	+^!t::puttext2("Util.GetPathInHierarchy(transform)")
	;+^!h::Header()
	;+!p::CSPublic()
	+^i::InsertClassAndMethodName()
	;+^i::addIncludeSmart()
	;^o::^;
	;+^p::InsertDebugPrint()
	+!p::InsertDebugPrint2()
	+^!p::LogVS2()
	;{::InsertOpenCurlyBracket()

	;^d::send ^c^v
	;+^g::addIncludeToTop("#include ""gaUtil/gaUtil.h""")
	;+^g::addIncludeToTop(PrintInclude)
	+^l::LogVS()
	^#::ToStringSurround()
	+enter::send {end}{return}
	;^#::FloatFix()
	+^down::selectBlock()
	;^.::PutText2(".instance.")
	+^!/::Bracket("/*", "*/")
#IfWinActive


#IfWinActive WhatsApp
	;daddy ↓
	^d::send 🧑🏻
	;lucky ↓
	^l::send 🐕
	;mummy ↓
	^m::send 👩🏻
	;praise ↓
	^p::send 👋🏻
	;workout ↓
	^e::send 🏋‍
	;coffee ↓
	^k::send ☕
	;done ↓
	^/::send ✅
	+^e::WhatsAppEmoji()
	+^a::bracket("*", "*")
	!up::send +{tab 2}{up}{tab 2}
	!down::send +{tab 2}{down}{tab 2}
#IfWinActive

#ifwinactive ahk_exe slack.exe
	^q::nop()
#IfWinActive

#IfWinActive ahk_exe firefox.exe
	^numpadmult::browserCloseTab()
	^numpad8::send ^{f5}
	+numpadmult::browserKey("+^t")
	~LButton::BrowserClick()
	+^c::browser_copy_url()
	;+^i::insertSheetRow()
	^numpaddiv::MyNextRssStory(1) ;inoreader
	+^numpaddiv::MyNextRssStory(0)
	;!left::FirefoxBack()
	;XButton1::FirefoxBack()
	xButton2 & rbutton::send +^t
	xButton2::send {xButton2}
#IfWinActive

#IfWinActive ahk_exe chrome.exe
	+^w::send ^i
	+^a::send ^b
	^tab::send !s
	~LButton::BrowserClick()
	!left::ChromeBack()
	XButton1::ChromeBack()
	;MButton::ChromeMiddle()
	;+^return::ipyNotebookEval()
	^numpadmult::ChromeBack()
	^k::ChromeBack()
	;+^::send ^{f4}
	^numpad8::send ^{f5}
	^numpad2::winminimize
	F8::browserCloseTab()
	+^c::browser_copy_url()
	+^i::insertSheetRow()
	+numpadmult::browserKey("+^t")
	^numpaddiv::MyNextRssStory(1) ;inoreader
	+^numpaddiv::MyNextRssStory(0)
	xButton2 & rbutton::send +^t
	xButton2::send {xButton2}
#IfWinActive

#ifwinactive SumatraPDF
	^numpadmult::send !{f4}
#ifwinactive

#ifwinactive PDF-XChange Viewer
	^g::send +^n+{tab}
	f2::send {alt}vlf ;one page
	f3::send {alt}vls ;2 pages
	f4::send {alt}vlc ;continuous
	;^t::send {alt} ; doesn't work
#ifwinactive

;#ifwinactive MixMeister
;	!b::send !b
;#ifwinactive

#IfWinNotActive TrueCrypt
	!d::send ^d
#IfWinActive 

#IfWinActive Oxford English Dictionary
	;!f4::send #{f4}
	!f4::winclose
	^numpad4::winclose
	^numpadmult::winclose
	esc::winclose
#IfWinActive 

#IfWinActive MonoDevelop
	^o::send ^,
#IfWinActive 

#ifwinactive Perforce P4V
	+^r::send +^!r
	^g::send +^g
#ifwinactive

#ifwinactive Submit Changelist
	^e::send scriptable object
#ifwinactive

#IfWinActive ahk_class UnityContainerWndClass
	;f5::vs_then("{f5}")
	;+f5::vs_then("+{f5}")
	;^o::+^o
	^e::sendinput +!a
	;^q::+^o
	!q::send +^p
	;^r::restartUnity()
	f5::Send ^p
	+f5::stopUnity()
	f9::send +^p
	f10::send ^!p
	f11::
		send ^4
		send {down}
		return
#IfWinActive

#IfWinActive Google Docs
	+^!0::unbold()
	#IfWinActive

#IfWinActive - Google Search
	; needs shortcut keys for google search firefox addon
	numpadsub::send !k
	numpadadd::send !j
	^down::send !j
	^up::send !k
	;numpadmult::send {enter}
	numpaddiv::send !b
#IfWinActive

;#IfWinActive reddit.com
;	numpadsub::send +k
;	numpadadd::send +j
;	numpad9::send k
;	numpad7::send {numpadenter}
;	numpad6::send j
;	numpadmult:: send {numpad1}
;#IfWinActive

#IfWinActive ahk_exe vivaldi.exe
	f9::Send +^p
#IfWinActive

;#IfWinActive Remote Desktop Connection
;	xbutton1::Sendplay !{left}
;#IfWinActive

#IfWinActive IPython ahk_class QWidget
	+^c::pythoncopy()
	^w::putTextI(";wa")
#IfWinActive

#IfWinActive Spyder (Python 3.6)
	+^c::pythoncopy()
	^w::putTextI(";wa")
	^enter::spy_retUp()
	+enter::spy_retDown()
	^r::spy_reset()
	!2::send +^i
	!0::send +^e
	+^f5::send ^{return}
	!left::send {end}{home}+{tab}
	!right::send {end}{home}{tab}
	^d::send ^c^v^v
	+^p::InsertPythonPrint()
	;^n::putTextI(";gmail.note")
	;^p::putTextI("!google.lnk ")
#IfWinActive

#IfWinActive ahk_class EVERYTHING
	;+^c::everythingCopyPath()
#IfWinActive

#IfWinActive ahk_class rctrl_renwnd32 ; outlook
	^numpaddiv::send ^q{up}
	numpaddiv::send ^q
	+^c::outlookCopyEmailTitle()
	^m::^t
	+^m::+^t
#IfWinActive

#IfWinActive - Message ahk_class rctrl_renwnd32 ; outlook message
	+^c::outlookCopyEmailTitle()
#IfWinActive

#IfWinActive ahk_exe explorer.exe
	+^c::clipboard := copyFullPath()
	^#e::change_to_dopus()
#IfWinActive

#IfWinActive Microsoft Word
	+^c::clipboard := copyFullPath()
#IfWinActive

#IfWinActive Microsoft Excel
	f5::excelRefresh(0)
	f6::excelRefresh(1)
	+^d::insertRow()
	^l::deleteRow()
	^w::excel_weight_entry()
#IfWinActive

#IfWinActive ahk_class XLMAIN
	+^d::insertRow()
	^l::deleteRow()
	^w::excel_weight_entry()
#IfWinActive

#IfWinActive powershell.exe ahk_class ConsoleWindowClass
	!f4::winclose
#IfWinActive

#IfWinActive Android ahk_class SunAwtFrame
	f10::typeClipboard()
#IfWinActive

#IfWinActive Windows fusion
	esc::enter
#IfWinActive

#IfWinActive Inoreader -
	numpadmult::send v
	numpaddiv::send f
	\::send m
	numpad8::send u
	numpad7::send o
	numpadadd::send n
	numpad4::send ga
	numpad5::send gf
	numpadsub::send p
	;^numpad9::togglerssdir()
	numpad1::opencomments()
#IfWinActive

#IfWinActive Adobe Flash Player ahk_class ShockwaveFlashFullScreen
	^numpadmult::browserCloseTab()
#IfWinActive

#IfWinActive ahk_class PotPlayer64
	^numpadmult::send !{f4}
#IfWinActive

#ifwinactive Beyond Compare
	+^!q::stampcdje()
	!q::ToggleCdje(3)
	^q::PutText2("/*cdje*/")
	!Backspace::BCSaveAndUp()
	^/::Bracket("/*", "*/")
#ifwinactive

#ifwinactive Folder Compare - Beyond Compare
	!left::send {left}
	!right::send {right}
#ifwinactive

#ifwinactive ahk_class gdkWindowToplevel ;pidgin
	;+!^#o::send {U+00F8} ; works with msdev, npp
	;+!^#o::sendPidginDanishO() ; e.g. soren
#ifwinactive

#IfWinActive MATLAB
	^d::matlabdup()
	!0::send ^0
	!1::send +^0
	^1::send +^0
	f8::matlabrun("ex8_cofi")
#IfWinActive

; console
#IfWinActive ahk_class ConsoleWindowClass
	^V::
	SendInput {Raw}%clipboard%
	return
#IfWinActive

#IfWinActive Sublime Text 2
	;f1::msgbox ^q
	;+^]::Bracket("[", "]")
	;+^!p::edit("C:\data\Dropbox\Packages\cde\cde.py")
	^numpadmult::nop()
	^numpad7::send ^{numpad7}
	!'::send ^{numpaddiv}
	+^#::send +^!{f2}
	;^numpad3::calcSchedule()
	;+^[::Bracket("[", "]")
	^.::PutText2("->")
	$+^5::send +^5
	^e::send scriptable object
	;+^5::msgbox
	f5::myReload()
#IfWinActive

#ifwinnotactive Sublime Text 2
	;+^0::puttext3(":)")
	;^!0::MyDateInline()
#ifwinnotactive

#IfWinActive JRiver Media Center ahk_class MJFrame
	numpad1::jr_focuslist()
#IfWinActive

;; convenient cursor control
	;$!i::send ^{left}

	!u::send ^{backspace}
	$!i::send {backspace}
	!o::send {delete}
	!p::send ^{delete}

	!g::send {home}
	^!g::send ^{home}
	+!g::send +{home}

	!h::send {left}
	^!h::send ^{left}
	+!h::send +{left}
	+^!h::send +^{left}

	!j::send {down}
	+!j::send +{down}
	+^!j::send !{down}

	!k::send {up}
	+!k::send +{up}
	+^!k::send !{up}

	!l::send {right}
	^!l::send ^{right}
	+!l::send +{right}
	+^!l::send +^{right}
	
	!;::send {end}
	^!;::send ^{end}
	+!;::send +{end}

	!n::send {pgdn}
	!m::send {pgup}

; fn row ----------------------------------------------------------------------------------
	;+^!{f1} !trillian
	f1::GotoURL(GetSelection())
	^f1::GotoURL("https://www.youtube.com/results?search_query=" . GetSelection(1))
	^!f1::ahkhelp()
	#f1::restart()
	#f2::standby()
	#f3::shutdown()
	;f11::minTest("C:\python26\scripts\ipython.exe ahk_class ConsoleWindowClass")
	#F4::WinKill, A
	+^F5::updateFinances()
	;f6::msgbox
	#^f6::globalCheats()
	;f8::temp()
	;#f8::WinActivate IS-NITRO-DEBUGGER
	+^!f8::DllCall("PowrProf\SetSuspendState", "int", 0, "int", 1, "int", 0)
	;F9::CheckGmote()
	;^!F9::Run % "D:\audio\streams\soma.beat blender.pls"
	;^!F10::Run % "D:\audio\streams\soma.covers.pls"
	;^!F11::winbigger()
	;^!f11::+^!f2
	#f10::WinHide A
	#f12::WinSet, AlwaysOnTop, toggle, A
	;*f13::badCtrl()
	;*f13::SOUNDbeep 400, 50
	!pause::send {esc}

; num row ----------------------------------------------------------------------------------
	;#1::stopUnity()
	#1::switchToUnreal()
	;#2::switchToAndRestartUnity()
	;#2::UeRun()
	^!$::PutText2("4493528115008639 0218 0719 049 chrdersk")
	^!#$::PutText2("4757147679118527 0218 0222 796 mrcder")
	; +^4:: office: code style
	+^2::Bracket("""", """")
	+^3::send {asc 156}
	:*:$$::pound
	+^!3::send {U+20ac} ; €
	#3::stopGameGotoVisualStudio()
	#4::perforce()
	^!#5::Explore(MyProgramFiles)
	^!5::Explore("c:\program files")
	;+^6::puttext2("*FIXED:* ")
	+^!6::Explore("c:\data\Shortcuts")
	; +^!6::Explore("C:\Documents and Settings\cerskine\Start Menu\Programs\Startup")
	;^!8::edit("C:\data\Dropbox\piano grade 3 study.txt")
	;+^9::bracket("*", "*")
	!9::send ^{f3}
	+!9::send +^{f3}
	+^9::AddBracketBefore()
	^!9::test()
	+^0::bracket("(", ")")
	^!+0::MyTomorrow()
	+^!-::send —
	^!=::SwapEquals()
	!backspace::send ^l

; qwertyuiop ----------------------------------------------------------------------------------
	^!tab::putText2("	")
	^!+tab::putText2("  - ")
	;$^q::home()
	^!q::Explore("::{20D04FE0-3AEA-1069-A2D8-08002B30309D}")
	+^q::autoType()
	;#q::tile(0)
	#q::switch_to_editor(0)
	#^q::VSThenFindCdjeInFiles()
	; ^!q::test()
	#w::LocateBrowserTab("WhatsApp")
	^!w::ExploreWork()
	;#w::
	;#w::winactivate Microsoft Word
	;$^e::end()
	; ^!+e::Run C:\data\Dropbox\autohotkey\iccHelper.ahk
	;#e::showOpus()
	+#e::Explore("::{20D04FE0-3AEA-1069-A2D8-08002B30309D}")
	^#e::run % "c:\program files\gpsoftware\directory opus\dopusrt.exe /cmd go new"
	^!e::explore("c:\data\dropbox")
	#!e::exploreCurrentFile()
	^!r::Run C:\Users\Christian\AppData\Local\OnTopReplica\OnTopReplica.exe,, Hide, PID
	+#r::FileRecycleEmpty
	+^!r::PasteReverse()
	;^#r::Run % "C:\Program Files\VS Revo Group\Revo Uninstaller Pro\RevoUninPro.exe"
	^#r::Run % "C:\apps\geek uninstaller\geek.exe"
	; ^#r::Run % "C:\apps\iobit uninstaller\iobit-uninstaller.exe"
	^#t::Run Timedate.cpl
	+#t::BrowserTab("https://mail.google.com/tasks/canvas?pli=1")
	^!t::edit("C:\data\Dropbox\Apps\Simpletask App Folder\todo.txt")
	;+^!t::edit("C:\data\Dropbox\todo\done.txt")
	;!t::songresults()
	+^!y::explore("C:\data\Dropbox\python")
	; ^!u::Speq()
	;#u::switchToUnity()
	; ^!u::python()
	^#u::edit(MyDropbox . "\autohotkey\util.ahk")
	+^i::bracket("_", "_")
	#i::Browser(0)
	#o::oed()
	;^!o::BrowserTab("https://mail.google.com/mail/#inbox")
	;+^!o::BrowserTab("https://mail.google.com/mail/#compose")
	; +!o::gmail notifier
	!^#o::danish_o()
	+!^#o::danish_o2()
	; +!^#o::Send {Asc 0248} ; works with msdev, npp
	; +!^#o::test()
	; +!^#o::PutText3("ø") ; works with -
	; +!^#o::Clipboard = (*UCP)ø ; works with -
	; +!^#o::ø ; works with -
	; +!^#o::send ø ; works with -
	^#o::BrowserTab("https://mail.google.com/mail/#contacts")
	!#o::BrowserTab("https://www.google.com/calendar/render?tab=mc")
	; #p::PhoneChat()
	;^!p::MyHack()
	;$^!p::keepass()
	;$^!p::temp()
	+^!p::genpass()
	;<!]::shiftalttab ;interferes with org-mode
	^![::braceBlock() ; careful of beyond compare
	+^[::InsertCurlyBraces()

; asdfghjkl ----------------------------------------------------------------------------------
	;+Capslock::togglecapslock()
	^!a::Explore("D:/audio")
	+^a::bracket("*", "*")
	;#s::skype()
	#a::sourceControl()
	;#a::winactivate DruinkClient
	#s::slack()
	^#s::run c:\windows\system32\control.exe mmsys.cpl`,`,0 ; sound control panel
	;+#s::DllCall("PowrProf\SetSuspendState", "int", 0, "int", 1, "int", 0)
	+^!d::GotoURL("http://en.wiktionary.org/wiki/" . GetSelection(1)) ; dictionary
	^!d::Explore("D:\")

	#f::StartWork()
	^#f::Finances()
	^!f:: puttext2("christian.erskine@gmail.com")
	+^!f:: puttext2("christian500@yahoo.co.uk")
	+^#f:: puttext2("christian@warhungryproductions.com")
	;^#!+f:: puttext3("oioisaveloyspam@yahoo.com")
	;^#!+f:: puttext2("cerskine@firebrandgames.com")
	; ^!g::GotoURL(1, 0) ; google clipboard
	; !#g::GotoURL(0, 0) ; goto clipboard
	;^!g::GotoURL(1, 1) ; google selection
	;#g::GotoURL(0, 1) ; goto selection
	+^!g::GotoURL("http://www.google.co.uk/search?complete=1&hl=en&q=" . GetSelection(1) . "&btnG=Google+Search")
	#g::BrowserTab("http://www.google.co.uk")
	;#h::spyder(0)
	#j::youtubeKey("j")
	^j::send ^z^y
	;^#j::opennotes("firebrand.txt", 0)
	^#j::toggleWorkNotes()
	^!j::WorkNotes(0)
	#!j::WorkNotes(2)
	;#!j::opennotes("work.org")
	;+^!#j::opennotes("kobojo.org")
	;#j::edit("")
	;#j::emacs_jump()
	;#l::youtubeKey("j")
	#l::youtubeKey("right")
	;$#k::show_trillian(1)
	^!k::edit(MyDropbox . "\autohotkey\main.ahk")
	^#l::myReload()
	^!#l::send +^!{f9}
	;+^#l::run "C:\Program Files\Oracle\VirtualBox\VirtualBox.exe" --comment "Lubuntu" --startvm "ae86711d-05fa-4f1f-b1c4-ce844de4fdcb"
	+^#l::linux()
	;+^#l::kittyLogin()
	##::soundbeep 440, 2000
	<!#::alttab
	>^#::alttab
	+^##::bracket("~", "~")
	;>#return::AltTab
	^!return::sublimeNewTodo()
	;<^>!#::alttab

; zxcvbnm ----------------------------------------------------------------------------------
	;<!\::alttab
	;!\::alttab
	#\::Browser(1)
	^\::send {return}
	;#z::#v
	^!z::Run % "c:/program files/TrueCrypt\TrueCrypt.exe"
	;^!z::Run % "C:\Program Files\VeraCrypt\VeraCrypt.exe"
	!z::send {esc}
	;^!+z::Run % "c:/program files/truecrypt\TrueCrypt.exe /d /b"
	;+#x::Explore("n:\")
	#z::switch_to_editor(0)
	+#x::Run, taskmgr,
	^!#x::Run % MyProgramFiles . "\Dropbox\Dropbox.exe"
	^!x::appendClip(0)
	+^!x::appendClip(1)
	;#x::switchToUnity()
	;#x::switchToUnreal()
	;#x::UeCompileRun()
	#x::linux()
	;#c::winactivate Visual Studio
	;#c::stopGameGotoVisualStudio()
	#c::codeapp()
	^!c::Explore("C:\")
	;+^!c::CopyBack()
	^#c::edit_current_file()
	;+^c:: assigned per app for copying current doc path to clipboard
	; +^v::PasteRaw()
	; ^!v::Run c:\windows\ehome\ehshell.exe
	; ^!v::Run "C:\Program Files (x86)\TightVNC\vncviewer.exe" 192.168.1.67:5901
	;+^!v::PutText3(%clipboard%)
	+!v::send +^!{f6} ; ditto . paste previous
	#v::ditto()
	+^!v::putClipboard()
	!x::send ^x
	$!c::send ^c
	!v::send ^v
	!b::send ^b
	;#b::showPidgin()
	;#b windows shortcut for focusing system notification area
	^!n::opennotes("home.txt", 0)
	;+^!n::BrowserTab("https://drive.google.com/keep/")
	; +^n::NewFolder()
	#!n::opennotes("home.txt", 1)
	#n::switch_to_editor(1)
	; ^#n::Run % "c:/program files\truecrypt\TrueCrypt.exe /v """ . MyDropbox . "\notes.tc"" /l n /q /e"
	;+!m ; vs extract method
	^#m::rdp()
	^!m::spyder(1)
	;#m::ipython()
	#m::LocateBrowserTab("- Gmail")
	+#m::nbsHotkey("+^!m")
	; #m::console()
	;!n::send {pgup}
	;+!n::send +{pgup}
	$!,::send ^l
	+^!.::send {U+2193} ;↓
	+^!,::send {U+2191} ;↑

; numeric pad ----------------------------------------------------------------------------------
	;^numpadmult::msgbox
	^numpadmult::send !{f4}
	+^numpadmult::numlock
	^numpad7::moveMouseAway()
	^#numpad7::mouseMove -50, -50, 0, R
	^#numpad8::mouseMove 0, -50, 0, R
	^#numpad9::mouseMove 50, -50, 0, R
	^#numpad4::mouseMove -50, 0, 0, R
	;^#numpad5::msgbox
	^#numpad5::mouseclick left
	^#numpad6::mouseMove 50, 0, 0, R
	^#numpad1::mouseMove -50, 50, 0, R
	^#numpad2::mouseMove 0, 50, 0, R
	^#numpad3::mouseMove 50, 50, 0, R

	^numpad5::leftClick()
	^numpad6::rightClick()
	;numpadmult::test()
	;#numpad0::tile(-1)
	;#numpaddot::tile(1)
	^numpaddot::toggleKeyboard()
	;#numpad1::tile2(-1, 1)
	;#numpad2::tile2(0, 1)
	;#numpad3::tile2(1, 1)
	;#numpad4::tile2(-1, 0)
	;#numpad5::tile2(0, 0)
	;#numpad6::tile2(1, 0)
	;#numpad7::tile2(-1, -1)
	;#numpad8::tile2(0, -1)
	;#numpad9::tile2(1, -1)
	^numpad4::send ^{f4}
	;+^numpad4::send +^{f4} doesn't work
	!numpad4::send !{f4}
	^numpad9::browser(1)
	;!numpadmult::send !{f4}

; other
	#space::youtubeKey("space")
	$!space::send {esc}
	+!space::!space
	;+space::send {esc}
	!#up::run C:\apps\display\display.exe /rotate:0
	!#down::run C:\apps\display\display.exe /rotate:180
	!#left::run C:\apps\display\display.exe /rotate:90
	!#right::run C:\apps\display\display.exe /rotate:270
	#down::winminimize A
	;#Up::ToggleWindowMax()
	;#Down::ToggleWindowMin()
	;~MButton & WheelDown::AltTab 
	;~MButton & WheelUp::ShiftAltTab
	;XButton1 & Wheelup::msgbox
	;xButton1 & Wheeldown::msgbox
	xButton1 & rbutton::GotoURL(GetSelection())
	xButton1::send {xButton1}
	;$xButton1::xButton1
	;AppsKey::msgbox

!delete::send ^l
;^delete::send +^{right}
;rwin & ~::ShiftAltTab
;^numpad1::msgbox
;+^return::send {down}{return}
+^;::MyTime()
^;::MyDateWee()
+^!;::MyDateAndTime()
;#;::foobar()
#;::music()
;+<!#::show_music()
^#n::Run ncpa.cpl
;^!'::Run D:\audio\playlists\new.m3u
+^-::AddSeparator()
;^!=::AddSeparator()
;+`::BrowserTab("")
!`::BrowserTab("")
;`::keypirinha()
;!`::BrowserTab("http://www.google.co.uk")
+^`::PutText2("``")
^.::PutText2("->")
; move to locdep ^!v::Explore("D:\video")
;PrintScreen::pauseRadioForAds2()
;PrintScreen::send +^!c
;scrolllock::pauseRadioForAds2(0)
scrolllock::toggleMute()
+scrolllock::pauseRadioForAds2(1)

pausing := 0
pausingRemote := 0

toggleMute()
{
	global pausingRemote
	if (pausingRemote == 1)
		nbsHotkey("+^!m")
	else
	{
		ifwinexist ahk_exe AIMP.exe
			send +!c
		else
			Run c:\apps\AIMP\AIMP.exe
	}
}

nbsHotkey(key)
{
	global PythonDir
	str := PythonDir . "\pythonw.exe C:\data\Dropbox\python\client.py " . """--hotkey " . key . """"
	Run % str
}

pauseRadioForAds2(remote)
{
	global pausingRemote
	pausingRemote := remote

	toggleMute()

	global pausing
	if (pausing == 1)
	{
		pausing := 0
		soundbeep 311*1, 100
		SetTimer, unpauseRadio2, off
		return
	}
	soundbeep 311*2, 100
	tm := 1000*60*2.5
	pausing := 1
	SetTimer, unpauseRadio2, %tm%
}

unpauseRadio2()
{
	toggleMute()

	soundbeep 311*1, 100
	SetTimer, unpauseRadio2, off

	global pausing
	pausing := 0
}

MyDateAndTime()
{
	;FormatTime, MyDate,, dd MMM yyyy
	FormatTime, MyDate,, yyyy-MM-dd HH:mm
	ifwinactive Google Docs
		Send ^b%MyDate%^b{Enter}{Enter 1}{up 1}
	else ifwinactive Microsoft Visual Studio
		puttext3(MyDate)
	else
		puttext2(MyDate)
}

MyTime()
{
	;FormatTime, MyDate,, dd MMM yyyy
	FormatTime, MyDate,, HH:mm
	;ifwinactive Google Docs
	;	Send ^b%MyDate%^b{Enter}{Enter 1}{up 1}
	;else 
	ifwinactive Microsoft Visual Studio
		puttext3(MyDate)
	else
		puttext2(MyDate)
}

MyDateInline()
{
	FormatTime, MyDate,,ddd dd MMM yyyy
	puttext2(MyDate)
}

MyTomorrow()
{
	tomorrow =  ; Make it blank so that the below will use the current time.
	tomorrow += 1, days

	FormatTime, MyDate, %tomorrow%,yyyy-MM-dd
	Send,  %MyDate%
	Return
}
  

ClickExpandAll()
{
	Send ^f
	Sleep 100
	Send Expand All
	Send +{TAB}
	Send {TAB}
	Send {RETURN}
	Return
}

MyHack()
{
	Send {F3}
	Send {Home}
	Send {Home}
	Send +{Down}
	Send ^v
	Return
}

MyFind()
{
	Send ^f
	Send Done
	Send {RETURN}
	Send {ESCAPE}
	Return
}

GoogleSearch()
{
	Send {Home}
	Send g{space}
	Send {Return}
}

NewFolder()
{
	global Dopus
	clipsave := clipBoardAll
	InputBox, UserInput, Folder Name, Please enter a folder name., 
	if (UserInput = "")
		return
	Sleep 100
	;Send !d
	SendInput  {f4}
	Sleep 500
	if (Dopus == 0)
		SendInput {escape}
	Sleep 100
	SendInput ^c
	Sleep 50
	;Msgbox FileCreateDir % clipboard . "|" . %UserInput%
	folder := clipboard
	StringRight last, folder, 1
	if (last == "\")
		StringLeft folder, folder, StrLen(folder) - 1
	FileCreateDir % folder . "\" . UserInput
	Sleep 200
	if (Dopus == 0)
	{
		SendInput  {f4}
		SendInput {escape}
		Sleep 200
	}
	PutText2(folder . "\" . UserInput)
	SendInput {return}
	sleep 51
	clipboard := clipsave
}

Speq()
{
	global MyProgramFiles
	IfWinExist SpeQ
		WinActivate
	else
		Run % MyProgramFiles . "\SpeQ Mathematics\SpeQ Mathematics.exe"
}

OpenNotes(notes, action)
{
	if (action == 2)
	{
		clipsave := clipBoardAll
		clipboard :=

		Send ^c
		ClipWait, 1
	}

	opentruecrypt()

	edit("N:\" . notes)

	if (action >= 1)
	{
		send ^{home}
		send ^{return}
	}

	if (action == 2)
	{
		sleep 50
		send ^v
		;msgbox % clipboard
		;puttext2(%clipboard%)
	}
}

WixKill_old()
{
	DetectHiddenWindows, On
	TI := TrayIcons( "WixKill.exe" )
	StringSplit,TIV, TI, |
	uID  := RegExReplace( TIV4, "uID: " )
	Msg  := RegExReplace( TIV5, "MessageID: " )
	hWnd := RegExReplace( TIV6, "hWnd: " )

	;MsgBox uID: %uID%, Msg: %Msg%, hWnd: %hWnd%

	PostMessage, Msg, uID,0x204,, ahk_id %hWnd% ; Right Click down
	PostMessage, Msg, uID,0x205,, ahk_id %hWnd% ; Right Click Up
	Send x
}
 
GetFileProperty()
{
	SendInput !{return}
	winName := filename . " Properties"
	WinWait %winName%
	DetectHiddenText, On
	SetTitleMatchMode, Slow
	WinGetText, text,
	bob = 0
	Loop, parse, text, `n, `r
	{
		if A_Index = 10
		{
			path = %A_LoopField%
			break
		}
	}
	path := path . "\" . filename
	SendInput {esc}
	return path
}

NppFindHighlighted(back)
{
	clipsave := clipBoardAll
	clipboard :=

	Send ^c
	ClipWait, 1
	Send ^f
	Sleep 20
	Send ^v
	Sleep 50
	Send {return}
	Sleep 10
	Send {esc}
	if (back)
	{
		Send +{f3}
		Send +{f3}
	}
	
	sleep 51
	clipboard := clipsave
}

NppReformat(num)
{
   clipsave := clipBoardAll
   
   clipboard := num
   Sleep 50
   Send ^{F8}
   ;Send {tab}{left}.{delete}{space}
   Sleep 50
   
	sleep 51
	clipboard := clipsave
}

PasteRaw()
{
	KeyWait Shift
	KeyWait Control
	sleep 50
	Send #v
	Sleep 50
	Send +{enter}
}

BCSaveAndUp()
{
	SendInput !{pgdn}
	Sleep 50
	SendInput !y
	Sleep 200
	SendInput {esc}
}

nppFind(text)
{
	sendinput ^f
	Sleep 50 ; 100
	puttext2(text)
	Sleep 10 ; 10
	SendInput {return}
	Sleep 100 ; 100
	ifwinactive Find ahk_class #32770, Can't find the text
	{
		send {esc 2}
		return 0
	}
	
	send {esc}
	return 1
}

PhoneChat()
{
	SendEvent ^!+{f1}
	keywait lwin
	keywait rwin
	Sleep 200
	ifwinnotactive Buddy List
		return
	Send ^f
	sleep 1000
	Send Phone
	send {return}
	sleep 50
	send ^a
	sleep 50
	ifwinactive Accounts
	{
		send {esc}
		return
	}
	send {delete}
}

StartSMS(recip)
{
	send {tab}
	Send ^a
	Send {del}
	;sleep 1000
	PutText2("sms:" . recip . ":")
}

UndoRedo()
{
	Send ^z
	
	IfWinActive, CodeWarrior
		Send +^z
	else IfWinActive, MonoDevelop
		Send +^z
	else 
		Send ^y
}

dupPane()
{
	msgbox
	Send ^o^i
}

typeClipboard()
{
	text := clipboard
	puttext3(text)
}

rdp()
{
	keywait lwin
	keywait rwin
	keywait lcontrol
	keywait rcontrol
	Run "D:\stuff\old\NET\remote desktop\shuttle.rdp"
}

winbigger()
{
	WinGetPos,x,y, Width, Height, A
	WinMove, A,,x+5,y, Width + 20, Height
	;winmove, A, , x+5, y, 100, 100
}

togglecapslock()
{
   state := GetKeyState("Capslock", "T")   
   if (state = True)
      SetCapsLockState, off
   else
      SetCapsLockState, on
}

console()
{
	ifwinexist ahk_class Console_2_Main
	{
		winmove Figure 1 ahk_class TkTopLevel,,980, 0, 700, 700
		winactivate Figure 1 ahk_class TkTopLevel

		winmove ahk_class Console_2_Main,, 0, 0, 982, 1014
		winactivate ahk_class Console_2_Main
			
		return
	}

	;Run % "C:\apps\Console2\Console.exe -t console -t pylab"
	Run % "C:\apps\Console2\Console.exe -t console"
	winwaitactive ahk_class Console_2_Main
	sleep 100
	winmove A,, 0, 0, 982, 1014
}

appendClip(hierarchy)
{
	clipsave := ClipBoard
	;sleep 50
	if (hierarchy)
		Send +^x
	else
		Send ^c
	sleep 50
	StringRight, out, clipsave, 1
	if (out == "`n")
		ClipBoard := clipsave . ClipBoard
	else
		ClipBoard := clipsave . "`n" . ClipBoard
	StringRight, out, ClipBoard, 1
	if (out != "`n")
		ClipBoard := ClipBoard . "`n"
}

;appendClip()
;{
;   bak = %clipboard%
;   Send, ^c
;   clipboard = %bak%`r`n%clipboard%
;}

pythoncopy()
{
	puttext2("pyreadline.clipboard.set_clipboard_text(_)")
	sleep 50
	send {return}
}

collect()
{
	clipsave := clipBoardAll
	clipboard :=
	
	send ^c
	clipwait 0.5
	edit("c:\stuff\collect.txt")
	sleep 200
	send ^{end}
	sleep 50
	send ^v
	send {return}
	send +{home}
	send {backspace}
	sleep 500
	send ^{tab}
	
	sleep 51
	clipboard := clipsave
}

exploreCurrentFile()
{
	path := copyFullPath()
	SplitPath, path,, dir
	;msgbox % dir . "hi"
	;Run % dir
	run % "explorer.exe " . dir
}

showOpus()
{
	ifwinexist ahk_class dopus.lister
		winactivate
	else
		explore("c:\stuff")
}

everythingCopyPath()
{
	Send {AppsKey}
	sleep 50
	send f
}

; puts given text with a manual space after (ipython paste problem workaround)
putTextI(text)
{
	puttext2(text)
	putText3(" ")
}

gmailReplyBlank(title)
{
	ifwinnotactive % title
		return
		
	send r
	sleep 1000
	send ^a
	sleep 20
}

showPidgin()
{
	Process, Exist, pidgin.exe
	pidgin_pid = %errorLevel%

	If (pidgin_pid = 0)
	{
		startPidgin()
		sleep 1000
		showPidgin()
	}
	else
	{
		SendEvent +^!{f1}
		sleep 50
		WinActivate Buddy List ahk_class gdkWindowToplevel
	}
}

browser_copy_url()
{
	clipboard := ""
	ret := focus_url_bar()
	sleep 50
	send {esc}
	send ^c
	clipwait
	send {esc}{tab}
	WinGetTitle, t, A
	t := StrReplace(t, " - Google Chrome")
	s := clipboard . " (" . t . ")"
	clipboard := s
	;msgbox %clipboard%
	;msgbox % s
}

nppInsertLine()
{
	send !{home}
	send {left}{return}{space}+{home}{del}	
}

nppCutToNewLine()
{
	clipsave := clipBoardAll
	clipboard = 
	
	send ^x
	clipwait 1
	send {return}
	sleep 10
	send ^v

	sleep 51
	clipboard := clipsave
}

danish_o()
{
	ifwinactive ahk_class gdkWindowToplevel
		puttext2(chr(248))
	else
		send {U+00F8} ; danish o, works with msdev, npp
}

danish_o2()
{
	ifwinactive ahk_class gdkWindowToplevel
		puttext2(chr(216))
	else
		send {U+00D8} ; danish o, works with msdev, npp
}

firefoxCopyLinkURL()
{
	Send +{f10}
	sleep 50
	send a{return}
}

pound()
{
	ifwinactive ahk_class gdkWindowToplevel
		puttext2(chr(163))
	else
		send {asc 0163} ; £
}

reopenTabFirefox()
{
	;controlclick , x50 y170
	;msgbox
	escape_flash()
	send +^t
}

escape_flash_old()
{
	send {escape}
	sleep 50
	;controlclick , x50 y150, ahk_class MozillaWindowClass
	;mouseclick ,, 50, 100
}

darken_firefox()
{
	send +^!{f10}
	send 1
}

ipyNotebookEval()
{
	send +{return}
	sleep 50
	send {up}
}

edit_current_file()
{
	path := copyFullPath()
	edit(path)	
}

show_aimp()
{
	Process, Exist, AIMP3.exe
	status = %ErrorLevel% 
	if status != 0 
		send +!;
	else
	{
		global MyProgramFiles
		Run % MyProgramFiles . "\aimp3\aimp3.exe"
	}
}

foobar()
{
	Process exist, foobar2000
	status = %ErrorLevel% 
	if status != 0
	{
		ifwinactive foobar2000
			winminimize foobar2000
		else
			winactivate foobar2000
	}
	else
	{
		Run C:\Program Files (x86)\foobar2000\foobar2000.exe
	}
}

Clementine()
{
	Process exist, clementine.exe
	status = %ErrorLevel% 
	if status != 0
	{
		ifwinactive ahk_exe clementine.exe
		{
			;msgbox active
			winminimize ahk_exe clementine.exe
		}
		else
		{
			;msgbox inactive
			winactivate ahk_exe clementine.exe
		}
	}
	else
	{
		Run C:\Program Files (x86)\Clementine\clementine.exe
	}
}

music()
{
	Process exist, AIMP.exe
	status = %ErrorLevel% 
	if status != 0
	{
		ifwinactive ahk_exe AIMP.exe
		{
			;msgbox active
			winminimize ahk_exe AIMP.exe
		}
		else
		{
			;msgbox inactive
			winactivate ahk_exe AIMP.exe
		}
	}
	else
	{
		Run c:\apps\AIMP\AIMP.exe
	}
}

show_music_m()
{
	;WinGet, Process, ProcessName, A
	;MsgBox %Process%
	Process exist, MusicBee.exe
	status = %ErrorLevel% 
	if status != 0
	{
		ifwinactive MusicBee
			winminimize
		else
			winactivate MusicBee
	}
	else
	{
		Run C:\Program Files (x86)\MusicBee\MusicBee.exe
	}
}

show_trillian(toggle)
{
	Process, Exist, trillian.exe
	status = %ErrorLevel% 
	if status != 0 
	{
		ifwinactive Trillian ahk_class icoTrilly
		{
			if (toggle)
				send +^!{f3}
		}
		else
		{
			ifwinexist Trillian ahk_class icoTrilly
				winactivate
			else
				send +^!{f3}
		}
	}
	else
	{
		global MyProgramFiles
		cmd := MyProgramFiles . "\trillian\trillian.exe", MyProgramFiles . "\Trillian"
		;msgbox % cmd
		Run % cmd
		loop 200
		{
			ifwinexist Trillian ahk_class icoTrilly
				winactivate
			else
				send +^!{f3}
			ifwinactive
				break	
			sleep 250
		}
	}
}

change_to_dopus()
{
	send {f4}
	clipsave := clipBoardAll
	clipboard :=
	Send, {esc}^c
	ClipWait 1
	file := Clipboard
	sleep 51
	clipboard := clipsave

	;msgbox % file
	;return
	
	IfWinActive ahk_class CabinetWClass ; explorer
		send !{f4}
	Explore(file)
}

badCtrl()
{
	soundbeep 600, 50
	blockinput on
	keywait f13
	blockinput off
	send {shift up}
	send {alt up}
	send {ctrl up}
	soundbeep 500, 50
}

findFromSearchbox()
{
	send ^k
	send ^c
	send ^f
	send ^v
}

switch_to_editor(new)
{
	global MyEditorName
	ifwinexist % MyEditorName
		winactivate % MyEdtiorName
	else
		edit("")

	if (new)
		send ^n
}

test()
{
	msgbox test
}

func_nav()
{
	send ^{f8}
	send {tab}
	send {down}
}

FloatFix()
{
	clipsave := clipBoardAll
	clipboard :=
	
	Send ^c
	ClipWait, 0.2
	string := Clipboard       ; Convert to text

	mulPos := InStr(string, "*")
	divPos := InStr(string, "/")
	if (InStr(string, "float"))
		StringReplace string, string, float, fx32, All
	else if (mulPos && (!divPos || mulPos < divPos))
	{
		pos := mulPos
		spaceBefore = 0
		if (SubStr(string, pos-1, 1) == " ")
			spaceBefore = 1
		eol = 0
		if (SubStr(string, StrLen(string), 1) == ";")
			eol = 1
		;MsgBox %eol%
		string := "elMath::mul(" . SubStr(string, 1, pos - 1 - spaceBefore) . "," . SubStr(string, pos + 1, StrLen(string) - (pos + eol)) . ")"
		if (eol)
			string := string . ";"
	}
	else if (divPos)
	{
		pos := divPos
		spaceBefore = 0
		if (SubStr(string, pos-1, 1) == " ")
			spaceBefore = 1
		eol = 0
		if (SubStr(string, StrLen(string), 1) == ";")
			eol = 1
		;MsgBox %eol%
		string := "elMath::div(" . SubStr(string, 1, pos - 1 - spaceBefore) . "," . SubStr(string, pos + 1, StrLen(string) - (pos + eol)) . ")"
		if (eol)
			string := string . ";"
	}
	else
	{
		eol = 0
		if (SubStr(string, StrLen(string), 1) == ";")
			eol = 1
		string := "fx(" . SubStr(string, 1, StrLen(string) - eol) . ")"
		if (eol)
			string := string . ";"
	}

	ClipBoard := string
	SendPlay ^v                       ; For best compatibility: SendPlay
	
	sleep 51
	clipboard := clipsave
}

InsertClassName()
{
	KeyWait Alt
	class := GetClassName2()
	PutText2(class)
}

InsertMethodName()
{
	KeyWait Alt
	PutText2(VsGetMethodName())
}

InsertClassAndMethodName()
{
	KeyWait Alt
	str:= GetClassName2() + "." + VsGetMethodName()
	PutText2(str)
}

addIncludeSmart()
{
	clipsave := clipBoardAll
	clipboard :=
	
	Send ^b^c
	ClipWait, 0.2
	string := ClipBoard

	;class := GetClassName2()
	
	if (SubStr(string, 1, 2) == "m_")
	{
		StringReplace, string, string, m_, ga, All
		lower := SubStr(string, 3, 1)
		StringUpper cap, lower
		string := SubStr(string, 1, 2) . cap . SubStr(string, 4, 99)
	}
	
	if (SubStr(string, 1, 3) == "get")
		StringReplace, string, string, get, ga, All

	folder := string
	StringReplace, folder, folder, Manager, , All
	StringReplace, folder, folder, Base, , All
	StringReplace, folder, folder, gaDebugTune, gaDebug, All

	addIncludeToTop("#include """ . folder . "/" . string . ".h""")
	
	Sleep 200
	len := strlen(string) + 4
	Send {left %len%}

	sleep 51
	clipboard := clipsave
}

addIncludeToTop(incString)
{	
	Send ^{home}
	Send ^f
	Sleep 100

	PutText2("#include ""ga")
	Sleep 100
	Send {return}
	Sleep 100
	SendInput {escape}+{f3}{end}{return}
	Sleep 500
	;msgbox % "#include """ . folder . "/" . string . ".h"""
	PutText2(incString)
}

SwapEquals()
{	
	;run pythonw.exe C:\data\Dropbox\python\msgbox.py hello
	;send ^s
	clipsave := clipBoardAll
	clipboard := ""

	Keywait Alt
	sleep 50

	Send ^x
	ClipWait, 0.2
	string := ClipBoard

	FileDelete, c:\data\Dropbox\python\swapequals_in.txt
	FileAppend, %string%, c:\data\Dropbox\python\swapequals_in.txt
	runwait pythonw.exe C:\data\Dropbox\python\swapequals.py, C:\data\Dropbox\python\

	FileRead, string, c:\data\Dropbox\python\swapequals_out.txt

	if (string != "")
	{
		if (type == 3)
			PutText3(string)
		else
			PutText2(string)
	}
		
	sleep 51
	clipboard := clipsave
}

ToggleCdje(type)
{	
	;send ^s
	clipsave := clipBoardAll
	clipboard := ""

	Keywait Alt
	Send {home}+{end}
	sleep 50

	Send ^c
	ClipWait, 0.2
	string := ClipBoard

	StringGetPos, pos1, string, /*cdje*/
	
	if (pos1 = -1)
	{
		StringGetPos, pos1, string, true
		StringGetPos, pos2, string, false
		if (pos1 > -1 && (pos2 == -1 || pos1 < pos2))
		{
			string := SubStr(string, 0+1, pos1) . "false/*cdje*/" . SubStr(string, pos1 + 4+1)
		}
		else if (pos2 > -1)
		{
			string := SubStr(string, 0+1, pos2) . "true/*cdje*/" . SubStr(string, pos2 + 5+1)
		}
		else
		{
			StringGetPos, pos2, string, //
			if (pos2 == -1)
			{
				send {home}
				send ///*cdje*/
				string := ""
			}
			else
			{
				string := SubStr(string, 0+1, pos2+1) . "*cdje*" . SubStr(string, pos2 + 1+1)
				
			}
		}
	}
	else
	{
		if (SubStr(string, pos1-4+1, 4) == "true")
			string := SubStr(string, 0+1, pos1 - 4) . "false" . SubStr(string, pos1 + 8+1)
		else if (SubStr(string, pos1-5+1, 5) == "false")
			string := SubStr(string, 0+1, pos1 - 5) . "true" . SubStr(string, pos1 + 8+1)
		else 
		{
			StringGetPos, pos2, string, ///*cdje*/
			if (pos2 == -1)
			{
				string := SubStr(string, 0+1, pos1+1) . SubStr(string, pos1 + 7+1)
			}
			else
			{
				send {home}
				send +{right 10}
				send {backspace}
				string := ""
			}
		}
	}

	if (string != "")
	{
		if (type == 3)
			PutText3(string)
		else
			PutText2(string)
	}
		
	sleep 51
	clipboard := clipsave
}

FindCdjeInFiles()
{
	SendInput +^f
	Sleep 300
	PutText2("cdje")
	Sleep 50
	SendInput {return}
}

FindCdje()
{
	SendInput ^f
	Sleep 200
	PutText2("cdje")
	Sleep 20
	SendInput {return}
}

GetClassName2()
{
	name := copyFullPath()

	StringGetPos, pos, name, .
	if (pos = -1)
		return ""

	StringLeft name, name, pos

	StringGetPos, pos, name, \, R1
	if (pos = -1)
		return name

	StringRight name, name, StrLen(name) - pos - 1
	
	return name
}
		
mono_debug()
{
	Send ^{f6} ;attach to debugger
	sleep 100
	ifwinactive Attach to Process
	{
		send +{tab}{return} ;select attach button
		winactivate Unity -
	}
}

home()
{
	ifwinactive Microsoft Outlook
	{
		send ^q
		return
	}
	send {ctrl up}{home}
}

end()
{
	ifwinactive Perforce
	{
		send ^e
		return
	}

	ifwinactive Ditto ahk_class QPasteClass
	{
		send ^e
		return
	}

	send {ctrl up}{end}
}

selectBlock()
{
	send {down}{end}
	send +^]
	send +{end}+{right}
	send !a
	send +{up}+{end}+{home 2}
	send !a
	;send ^k^a
}

newTab()
{
	escape_flash()
	send ^t
	sleep 50
	;focus_url_bar()
}

jr_focuslist()
{
	send ^5
	;sleep 200
	send ^1
}

ToggleWindowMax()
{
	WinGet, already, MinMax, A
	if (already)
		WinRestore, A
	else
		WinMaximize, A
}

ToggleWindowMin()
{
	WinMinimize, A
}


comments()
{
	keywait ctrl
	send {alt}b
	sleep 100
	send .
	send 1
}

arsNext()
{
	keywait ctrl
	send {alt}b
	sleep 100
	send .
	send 2
}

calcSchedule()
{
	;sleep 100
	;send ^v
	;return
	send ^{home}
	send ^ftotal:{escape}
	send +^{home}
	sleep 300

	send ^fday
	send !{return}
	send {left}
	send +{home}^c
	send ^ftotal:{escape}
	send {right}+{end}{return}
	send {space}+{home}{backspace}
	;clipboard := "abc"
	sleep 100
	str := clipboard
	StringReplace,str,str,`n,+%A_Space%,A
	StringReplace,str,str,`r,,A
	StringReplace,str,str,%A_Tab%,,A
	;msgbox % str
	clipboard := str
	sleep 100
	send ^v
	send +{home}!=
	send {home}{backspace}{space}
	return
}


autotype()
{
	;soundbeep 440, 30
	Suspend On
	send +^!{f7}
	sleep 3000
	Suspend Off
}

putClipboard()
{
	str := clipboard
	;msgbox % str
	puttext3(str)
}

stampcdje()
{
	;str := " // firebrand/christian "
	;FormatTime, date2,, yyyy-MM-dd/HH:mm
	;str := str . date2

	str := "// WARHUNGRY PRODUCTIONS "
	puttext3(str)
	mydatewee()
}

genpass()
{
	Length = 10
	Characters = abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789
	pw := ""
	Loop %Length% 
	{
		Random, r, 1, % StrLen(Characters)
		pw .= SubStr(Characters, r, 1)
	}
	puttext3(pw)
	clipboard := pw
}

excel_weight_entry_macro()
{
	keywait ctrl
	clipsave := clipboardall

	send {esc}	
	send {down}
	insertRow()
	
	send {up}{home}
	
	;send +{space}
	send {right}+{right 4}

	sleep 400
	clipboard := 
	sleep 400
	send ^c
	sleep 400
	send {down}
	sleep 400
	send ^v
	sleep 1000 ;5000
	sleep 200

	send {up}
	sleep 200
	mydatewee()
	sleep 200
	send {tab}{delete}

	send {right 4}
	send +{right 3}
	send {delete}
	send {left 4}

	clipboard := clipsave

	return
}

excel_weight_entry()
{
	send ^w ; define macro in excel that does excel_weight_entry_macro
	send {home}{right}
	mydatewee()
	send {right}
}

max(x, y)
{
	return x > y ? x : y
}

min(x, y)
{
	return x < y ? x : y
}

tile2(x, y)
{
	WinGet, win1, ID, A
	WinGetPos, x1, y1, w1, h1, ahk_id %win1%
	;msgbox % x1 . " " . y1 . " " . w1 . " " . h1
	SysGet, MonitorWorkArea, MonitorWorkArea, 1
	ws := A_ScreenWidth
	hs := MonitorWorkAreaBottom
	x1 := max(x1, 0)
	y1 := max(y1, 0)
	w1 := min(w1, ws)
	h1 := min(h1, hs)
	;msgbox % x1 . " " . y1 . " " . w1 . " " . h1
	d  := 240

	if (x == 0 && y == 0)
	{
		winmaximize ahk_id %win1%
		return
	}
	fw := w1 == ws && x1 == 0
	if (w1 < ws || (fw && x < 0))
		w1 += d*x
	else
	{
		x1 += d*x
		w1 -= d*x
	}
	fh := h1 == hs && y1 == 0
	if (h1 < hs || (fh && y < 0))
		h1 += d*y
	else
	{
		y1 += d*y
		h1 -= d*y
	}

	winrestore ahk_id %win1%
	winmove ahk_id %win1%, , x1, y1, w1, h1
}

tile(dir)
{
	WinGet, id, list,,, Program Manager
	WinGet, actid, ID, A
	loop %id%
	{
		i = %A_index%
		this_id := id%A_Index%
		if (this_id == actid)
			break
	}
	win1 := id%i%
	i := i + 1
	win2 := id%i%
	WinGetPos, x1, y1, w1, h1, ahk_id %win1%
	WinGetPos, x2, y2, w2, h2, ahk_id %win2%
	if (w1 > A_ScreenWidth * 0.8)
		w1 := A_ScreenWidth * 0.8
	m1 := x1 + w1/2
	m2 := x2 + w2/2

	if (dir == 0)
	{
		SysGet, MonitorWorkArea, MonitorWorkArea, 1
	    ;MsgBox, %MonitorWorkAreaBottom%
		if (m1 > A_ScreenWidth / 2)
		{
			;soundbeep 200, 200
			x3 := 0
			y3 := 0
			w3 := x1
			h3 := MonitorWorkAreaBottom
		}
		else
		{
			;soundbeep 1000, 200
			x3 := x1 + w1
			y3 := 0
			w3 := A_ScreenWidth - x3
			h3 := MonitorWorkAreaBottom
		}
		winrestore ahk_id %win1%
		winmove ahk_id %win1%, , x1, y1, w1, h1
		winrestore ahk_id %win2%
		winmove ahk_id %win2%, , x3, y3, w3, h3
		;sleep 200
	}
	else
	{
		d := 240 * dir
		if (m1 < m2)
		{
			WinGetPos, x1, y1, w1, h1, ahk_id %win2%
			WinGetPos, x2, y2, w2, h2, ahk_id %win1%
			temp := win2
			win2 := win1
			win1 := temp
		}
		;msgbox % (x2 + w2) - x1
		joined := abs((x2 + w2) - x1) < 1
		x1 := x1 + d
		w1 := w1 - d
		if (joined)
			w2 := w2 + d
		winmove ahk_id %win1%, , x1, y1, w1, h1
		winmove ahk_id %win2%, , x2, y2, w2, h2
	}
}

braceBlock()
{
	send {return}{{}{return}{}}{up}{return}
}

songresults()
{
	send #h
	sleep 100
	send !2^a^c#j
	sleep 100
	send ^n
	sleep 200
	send ^v^a^{home}
	send ^f
	send section vols
	send {return}{esc}
	send !{f3}
	sleep 100
	send +^l+{left}^c
	sleep 100
	send ^a^v^{home}^{end}
	send {return}+{home}{backspace}
	send ^a{tab 2}^a^c
	send {alt up}
}

matlabdup()
{
	send {end}{home}{home}
	send {shift down}
	;sleep 20
	send {down}
	;sleep 20
	send {shift up}
	send {lctrl down}
	;sleep 20
	send {c}
	send {v 2}
	;sleep 20
	send {lctrl up}
	send {left}
}

ffoptions()
{
	send !t
	sleep 200
	send o
}

matlabrun(file)
{
	send +{f5}
	sleep 50
	send ^0
	sleep 50
	send ^c
	sleep 150
	send %file%
	send {return}
}

vs_then(key)
{
	winactivate Microsoft Visual Studio
	winwaitactive Microsoft Visual Studio
	sleep 50
	send % key
}

TuneSurround()
{
	clipsave := clipBoardAll
	clipboard :=
	
	Send ^c
	ClipWait, 0.2
	string := Clipboard       ; Convert to text
	if (string == "")
		string := "1"
	stringLen := StrLen(string)
	string := "/*cdje*/gaDebugTune::get("""", " . string . ")"

	ClipBoard := string
	SendPlay ^v                       ; For best compatibility: SendPlay
	Sleep 50                      ; Don't change clipboard while it is pasted! (Sleep > 0)

	num := stringLen + 4
	keywait control
	Send {left %num%}
	
	sleep 51
	clipboard := clipsave
}

CSPublic2()
{
	clipsave := clipBoardAll
	clipboard :=
	
	Send {end}{left}+^{left}
	send ^c
	ClipWait, 0.2
	var := Clipboard       ; Convert to text

	Send {home}+^{right}+{left}
	clipboard :=
	send ^c
	ClipWait, 0.2
	type := Clipboard       ; Convert to text

	send {end}{return}

	stringLen := StrLen(string)
	prop := Substr(var, 3)
	string := "public " . type . " " . prop . " { get { return " . var . "; } }"
	;msgbox % string
	;return
	puttext2(string)
	send {left}

	sleep 51
	clipboard := clipsave
}

CSPublic()
{
	clipsave := clipBoardAll
	clipboard :=
	
	send {end}{home}+{end}
	sleep 20
	send ^c
	ClipWait, 0.2
	line := Clipboard       ; Convert to text

	semi := ";"
	stringgetpos pos, line, %semi%
	line := substr(line, 1, pos)
	if substr(line, 1, 7) == "public "
		line := substr(line, 8)
	if substr(line, 1, 8) == "private "
		line := substr(line, 9)

	send {end}{return}
	puttext2(line)

	Send {end}+^{left}
	sleep 20
	clipboard :=
	send ^c
	ClipWait, 0.2
	var := Clipboard       ; Convert to text


	Send {home}+^{right}+{left}
	sleep 20
	clipboard :=
	send ^c
	ClipWait, 0.2
	type := Clipboard       ; Convert to text

	send ^l{left}{return}

	stringLen := StrLen(string)
	prop := Substr(var, 3)
	string := "public " . type . " " . prop . " { get { return " . var . "; } set { " . var . " = value; } }"
	;msgbox % string
	;return
	puttext2(string)
	send {left}
	send +^{left 7}

	sleep 51
	clipboard := clipsave
}

commonCppFile()
{
	send ^o
	sleep 100
	global commonCpp
	send %commonCpp%
	sleep 100
	send {return}
}

spy_retUp()
{
	send {return}{up}
}

spy_retDown()
{
	send {end}{return}
}

spy_reset()
{
	send !2
	send {`%}reset -f{return}
}

sublimeNewTodo()
{
	switch_to_editor(0)
	send ^{home}
	send ^{return}
}

skype()
{
	ifwinexist Skype
		winactivate Skype
	else
		run C:\Program Files (x86)\Skype\Phone\Skype.exe
}

slack()
{
	ifwinexist Slack
		winactivate Slack
	else
		run C:\Users\Christian\AppData\Local\slack\Update.exe --processStart slack.exe, C:\Users\Christian\AppData\Local\slack\app-2.0.3
}

switchToUnreal()
{
	ifwinexist ahk_exe UE4Editor.exe
		winactivate 
}

switchToUnity()
{
	ifwinexist ahk_class UnityContainerWndClass
		winactivate 
	else
	{
		run "C:\Program Files\Unity-2017\Editor\Unity.exe"
		winwaitactive ahk_class UnityContainerWndClass
	}
}

saveVSAndSwitchToUnity()
{
	IfWinActive Microsoft Visual Studio
	{
		send +^s
		sleep 500
	}
	stopUnity()
}

saveAndRestartUnity()
{
	send +^s
	sleep 500
	switchToAndRestartUnity()
}

debugAndRestartUnity()
{
	send +{f5}
	sleep 100
	send {f5}
	sleep 500
	switchToAndRestartUnity()
}

StartWork()
{
	slack()
	Browser()
	WorkNotes(0)
	visualStudio()
	;skype()
	;obs() disp1 screen can go wrong if started before connected to tv
	sourceControl()
	music()
	run C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Pushbullet\Pushbullet.lnk
	switchToUnity()
}

sourceControl()
{
	;ifwinexist Submit Changelist
	;	winactivate Submit Changelist
	;else
	;	perforce()
		;plastic()
	;ifwinexist Plastic SCM
	;	winactivate
	;else
	;	run C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Plastic SCM\Plastic SCM.lnk
	ifwinexist Sourcetree
		winactivate
	else
		run  C:\Users\Christian\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Atlassian\SourceTree.lnk
}

plastic()
{
	ifwinexist SourceTree
		winactivate
	else
		run C:\Program Files (x86)\Atlassian\SourceTree\SourceTree.exe

	loop, 15
	{
		;soundbeep 440, 100
		sleep 500
		ifwinexist SourceTree
		{
			winactivate
			break
		}
	}
}

restartUnity()
{
	stopUnity()
	sleep 1000
	Send ^p
}

stopUnity()
{
	switchToUnity()
	send ^u
}

stopUnityFromOtherApp()
{
	WinGet, current_ID, ID, A
	stopUnity()
	;visualStudio()
	WinActivate ahk_id %current_ID%
}

switchToAndRestartUnity()
{
	switchToUnity()
	restartUnity()
}

stopGameGotoVisualStudio()
{
	global Unity
	if (Unity)
		stopUnity()
	else
		send {esc}
	visualStudio()
}

restartDebugging()
{
	send +{f5}
	sleep 1000
	send {f5}
}

perforceLocateFromVS()
{
	c := getClassName2()
	c := c . ".cs"
	perforceLocate(C)
}

unbold()
{
	send ^!0
	send {home}+{end}
	send ^b
	send {home}{return}
}

temp2()
{
	send ^{home}
	sleep 50
	send ^fclass` {esc}
	send ^fusing+{f3}{esc}
	send {home}{down}{return}
	;send {return}{up} ; optional
	;send {return}{up 1}
	send namespace pr{return}
	send {{}{return}
	send {delete}
	send +^{end}{tab}
	send ^{end}{}}
	send ^{home}
}

temp_recompileAll()
{
	send +^{f7}
}

ahkhelp()
{
	global myahk
	Run % MyAHK . "/AutoHotkey.chm"
}

NewMethod()
{
	send `;
	send +{left}
	send +!m
}

nop()
{
}

Tooltip()
{
	Send % "[Tooltip("""")]"
	send {left 3}
	;send {return}{up}{end}{left 3}
}

Header()
{
	Send % "[Header("""")]"
	send {left 3}
}

globalCheats()
{
	VisualStudio()
	commonCppFile()
}

VSThenFindCdjeInFiles()
{
	VisualStudio()
	FindCdjeInFiles()
}

toggleWorkNotes()
{
	global work
	work := 1 - work
	soundbeep 311 * (2 - work), 50
}

WorkNotes(type)
{
	global work
	if (work == 0)
		opennotes("work.txt", type)
	else
		opennotes("old/firebrand.txt", type)
}

ExploreWork()
{
	global work
	if (work == 0)
		Explore("C:\projects\")
	else
		Explore("C:\projects\rhi\")
}

CopyBack()
{
	send +^{left}^c{right}
}

BrowserClick()
{
	If (A_TimeSincePriorHotkey<400) and (A_TimeSincePriorHotkey<>-1)
	{
		CoordMode, Mouse, Screen
		CoordMode, Pixel, Screen
		MouseGetPos, xpos, ypos 
		ypos := ypos + 1
		PixelGetColor c, %xpos%, %ypos%, Slow RGB

		;msgbox % "my ahk version: " A_AhkVersion
		;msgbox % xpos . " " . ypos
		;msgbox % c
		;if (ypos < 50) and (c == 0xF2F2F2)
		if (ypos < 50) and (c == 0xf9f9fa)
			send {f5}
	}
}

ChromeMiddle()
{
	MouseGetPos, xpos, ypos 
	if ypos < 50
	{
		MouseGetPos, xp2, yp2 
		PixelGetColor col, xp2, yp2, RGB
		;msgbox % xp2 . " " . yp2 . " " . col
		if (col == 0x5D5A58)
			send +^t
		else
			MouseClick, middle, xp2, yp2
	}
	else
		MouseClick, middle, xp2, yp2
}

ChromeBack2()
{
	WinGetTitle, t1, A
	Send {Browser_Back}
	return
	sleep 1500
	WinGetTitle, t2, A
	;msgbox % t1
	;msgbox % t2
	;if (t1 == t2)
	;	msgbox same
	;else
	;	msgbox diff
	if (t1 == t2)
		send ^{f4}
}

ChromeBack()
{
	;SysGet, VirtualScreenWidth, 78
	;SysGet, VirtualScreenHeight, 79
	;
	;SysGet, VirtualScreenLeft, 76
	;SysGet, VirtualScreenTop, 77
	;
	;msgbox VirtualScreen upper_left is %VirtualScreenLeft% %VirtualScreenTop%
	;msgbox VirtualScreen size is %VirtualScreenWidth% %VirtualScreenHeight%
	send {esc} ; to get out of youtube fullscreen
	sleep 100
	CoordMode, ToolTip, screen
	CoordMode, Mouse, screen
	CoordMode, Pixel, screen
	WinGetPos X, Y, Width, Height, A
	;Msgbox % X . " " . Y . " " . Width . " " . Height
	;return

	;imageSearch, foundx, foundy,   X, Y, X + 280, Y + 280, *55 C:\data\Dropbox\autohotkey\chrome back.png
	;imageSearch, foundx2, foundy2, X, Y, X + 280, Y + 280, *55 C:\data\Dropbox\autohotkey\chrome back 2.png
	imageSearch, foundx2, foundy2, X, Y, X + 150, Y + 150, *25 C:\data\Dropbox\autohotkey\chrome greyed back 2.jpg
	;found := foundx != "" || foundx2 != ""
	doClose := foundx2 != ""
	;if (foundx != "")
	;{
		;msgbox back button FOUND 1
		;return
	;}
	;if (foundx2 != "")
	;{
		;msgbox back button FOUND 2
		;return
	;}
	if (doClose)
	{
		;msgbox greyed back found
		;return
	    ; Icon could not be found on the screen.
		; msgbox sending back
	    send ^{f4}
	}
	else
	{
		;msgbox greyed back NOT found
		;return
		Send {Browser_Back}
		return
	    ; The icon was found at %FoundX%x%FoundY%.
		; msgbox sending close
	    ;send !{left}
	    mousegetpos mx, my
	    if (foundx != "")
	    {
		    foundx += 10
		    foundy += 10
		    click %foundx%, %foundy%, 1
		}
		else
	    {
	    	;msgbox %foundx2%
	    	;msgbox %foundy2%
		    foundx2 += 10
		    foundy2 += 10
		    click %foundx2%, %foundy2%, 1
		}
		;sleep 100
		click %mx%, %my%, 0
	}
}

FirefoxBack()
{
	CoordMode, Pixel
	;msgbox
	ImageSearch, foundx, foundy, 0, 0, 500, 500, *40 C:\data\Dropbox\autohotkey\firefox back.png
	;msgbox % foundx . " " . foundy
	;return
	found := foundx != ""
	if (found)
	{
		msgbox found
		return
	    send ^{f4}
	}
	else
	{
		msgbox not found
		return
		;msgbox back
		Send {Browser_Back}
	}
}

obs()
{
	run C:\Program Files (x86)\obs-studio\bin\64bit\obs64.exe, C:\Program Files (x86)\obs-studio\bin\64bit\
}

insertSheetRow()
{
	send !i
	sleep 100
	send r
}

RunWaitMany(commands) 
{
    shell := ComObjCreate("WScript.Shell")
    ; Open cmd.exe with echoing of commands disabled
    exec := shell.Exec(ComSpec " /Q /K echo off")
    ; Send the commands to execute, separated by newline
    exec.StdIn.WriteLine(commands "`nexit")  ; Always exit at the end!
    ; Read and return the output of all commands
    return exec.StdOut.ReadAll()
}

excelRefresh(copy)
{
	send ^{pgup}
	RunWaitMany("
(
C:\projects\ig\ig-webapi-dotnet-sample-master\Scrape\bin\Release\Scrape.exe
C:\Anaconda3\pythonw.exe C:\data\Dropbox\python\scrape.py
)")
	;runwait C:\projects\ig\ig-webapi-dotnet-sample-master\Scrape\bin\Release\Scrape.exe
	;runwait C:\Anaconda3\pythonw.exe C:\data\Dropbox\python\scrape.py
	if (copy)
		send ^q
	send ^!{f5}
}

formerly()
{
	PutText2("[UnityEngine.Serialization.FormerlySerializedAs("""")]")
	send {left 3}
}

FindRefsGotoDef()
{
	send {f12}
	sleep 750
	send +{f4}
	;send !1
	;send ^{f12}
}

AddBracketBefore()
{
	send {esc}
	send ^{left}
	send {(}
	send ^{right}
}

IfNonNull()
{
	send {end}{home}
	send +^{right}^c
	send {home}
	sendraw if (
	send ^v
	send {)}{return}{tab}
}

InsertCurlyBraces()
{
	send ^{return}{{}
	;send {down}+{return}{backspace}{}}
	send {return}
	send {}}

	send {down}!{up}{end}

	;send ^{delete}

	;send {up}{end}
	;send {return}{tab}
}

keypirinha()
{
	send +^!{f3}
	return
	sleep 20
	ifwinactive ahk_class keypirinha_wndcls_run
	{
		send {down}
		return
	}
	sleep 100
	ifwinactive ahk_class keypirinha_wndcls_run
		send {down}
}

WhatsAppEmoji()
{
	send +{tab}{enter}
}

DocumentPublicVars()
{	
	clipboard := ""
	Keywait Alt
	sleep 50

	Send ^c
	ClipWait, 0.2
	string := ClipBoard

	FileDelete, c:\data\Dropbox\python\DocumentPublicVars_in.txt
	FileAppend, %string%, c:\data\Dropbox\python\DocumentPublicVars_in.txt
	runwait pythonw.exe C:\data\Dropbox\python\DocumentPublicVars.py, C:\data\Dropbox\python\

	FileRead, string, c:\data\Dropbox\python\DocumentPublicVars_out.txt
	ClipBoard := string
}

moveMouseAway()
{
	;winactivate Inoreader
	;keywait ctrl
	;mouseclick left, 400, 900
	;sleep 300
	;mouseclick left, 400, 900
	mousemove 1000, 20, A
}

updateFinances()
{
	ifwinnotexist Excel - finances
		return

	winactivate Excel - finances
	excelRefresh(0)
}

restart()
{
	run C:\Users\Christian\Desktop\f1-restart.bat
}

standby()
{
	run C:\Users\Christian\Desktop\f2-standby.bat
}

shutdown()
{
	run C:\Users\Christian\Desktop\f3-shutdown.bat
}

Finances()
{
	if !FileExist("N:\")
		opentruecrypt()

	run n:/finances.xlsm
}

opentruecrypt()
{
	global MyDropbox
	If !FileExist("N:\")
	{
		global MyProgramFiles
		str := "c:/program files/truecrypt\TrueCrypt.exe /v """MyDropbox . "\todo\notes.tc"" /l n /q"
		Run % str
		Loop, 20
		{
			;Winactivate Enter password for
			;Ifwinactive Enter password for
			;{
			;	autotype()
			;	;soundbeep 311*4, 100
			;}
			;else
			;	soundbeep 311*2, 100

			If FileExist("N:\")
				break
			Sleep, 500
		}
	}
}

Flip( Str) 
{
	Loop, Parse, Str
	nStr=%A_LoopField%%nStr%
	Return nStr
}

PasteReverse()
{
	str := clipboard
	PutText2(flip(str))
}

leftClick()
{
	keywait ctrl
	mouseclick left
}

rightClick()
{
	keywait ctrl
	mouseclick right
}

vscprint()
{
	puttext2("print(f"""")")
	send {left 2}
}

vscAddToWatch()
{
	send +!w
	send +{tab}
	send {tab}
	send {return}
}

VisualStudio()
{
	ifwinexist - Microsoft Visual Studio
		winactivate
	else
		run "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\devenv.exe"
}

VectorText()
{
	global Unity
	if (Unity)
		PutText2("Vector3")
	else
		PutText2("Vector2f")
}

UeCompileRun()
{
	send {f7}
	count := 2
	found := 0
	loop 120
	{
		sleep 500
		WinGetPos X, Y, Width, Height, A
		;Msgbox % X . " " . Y . " " . Width . " " . Height
		;imageSearch, foundx2, foundy2, 0, 0, Height, Width, *100 C:\data\Dropbox\autohotkey\visual studio test.jpg
		imageSearch, foundx2, foundy2, 0, 0, 500, Height, *100 C:\data\Dropbox\autohotkey\visual studio test.jpg
		;Msgbox % foundx2 . " " . foundy2 . " "
		;imageSearch, foundx2, foundy2, X, Y, X + Width - 400, Y + 400, *25 C:\data\Dropbox\autohotkey\visual studio compiled.jpg
		;if ErrorLevel = 2
		    ;MsgBox Could not conduct the search.
		;else if ErrorLevel = 1
		    ;MsgBox Icon could not be found on the screen.
		;else
		    ;MsgBox The icon was found at %foundx2%x%foundy2%.
		found := foundx2 != ""
		if (found)
		{
			;return
			;msgbox found
			switchToUnreal()
			sleep 1000
			send {f5}
			;sleep 1000
			send {f2}
			return
		}

		imageSearch, foundx2, foundy2, 0, 0, 500, Height, *100 C:\data\Dropbox\autohotkey\visual studio failed.jpg
		;Msgbox % foundx2 . " " . foundy2 . " "
		;imageSearch, foundx2, foundy2, X, Y, X + Width - 400, Y + 400, *25 C:\data\Dropbox\autohotkey\visual studio compiled.jpg
		;if ErrorLevel = 2
		    ;MsgBox Could not conduct the search.
		;else if ErrorLevel = 1
		    ;MsgBox Icon could not be found on the screen.
		;else
		    ;MsgBox The icon was found at %foundx2%x%foundy2%.
		found := foundx2 != ""
		if (found)
			return
	}
	;msgbox not found

}

UeRun()
{
	switchToUnreal()
	sleep 500
	send {esc}
	sleep 500
	send {f5}
}

kittyLogin()
{
	run % "C:\apps\kitty\kitty.exe -load lubuntu"
	sleep 500
	send christian{return}
	sleep 500
	send qwe123QWE{return}
	winMove, A,, 1272, 38, 1296, 1410
}

gsmerge()
{
	Send !i
	sleep 200
	send {right}m{enter}
}

LocateBrowserTab(tab)
{
	ifwinnotactive ahk_exe chrome.exe
		browser()
	sleep 250
	ifwinactive %tab%
		return
	focus_url_bar()
	sleep 250
	send %tab%
	sleep 10
	send {down}{tab}{return}
}

oed()
{
	run c:\apps\Oxford English Dictionary - 2nd Ed. Vers.4.0 (2009)\oed.lnk , c:\apps\Oxford English Dictionary - 2nd Ed. Vers.4.0 (2009)
	sleep 500
	winactivate Oxford English Dictionary
}

LogVS()
{
	;SplitPath, str, name, dir
	;stringleft class, name, StrLen(name) - 4
	;PutText2("/*cdje*/elLog(""" . class . "::\n"");")
	;msgbox % method

	global Unity
	if (Unity)
	{
		method := VsGetMethodName()
		PutText2("/*cdje*/Debug.LogFormat(this, ""{0}." . method . ": "", name);")
		keywait shift
		Send {left 9}
	}
	else
	{
		;PutText2("UE_LOG(LogTemp, Log, TEXT(""""));")
		;keywait shift
		;Send {left 4}

		;PutText2("/*cdje*/DebugLog(""\n"");")
		;keywait shift
		;Send {left 5}

		PutText2("/*cdje*/printf(""\n"");")
		keywait shift
		Send {left 5}
	}
}

GotoURL(string)
{
	str := string
	str := ltrim(str)
	prot := regexmatch(str, "https?\://") ; web url
	if (prot)
	{
		str := substr(str, prot)
		str := StrSplit(str, A_Space)[1]
	}

	if (substr(str, 2, 2) == ":\") ; windows file path
	{
		str := rtrim(str, "`r`n") ; newline can happen when pressing f1 with nothing selected
		str := """" . str . """" ; in case spaces
		;msgbox % str
		run % str
		return
	}

	prot := regexmatch(str, "file://") ; file url
	if (prot)
	{
		str := substr(str, prot)
		str := rtrim(str, "`r`n") ; newline can happen when pressing f1 with nothing selected
		str := substr(str, 9)
		str := strreplace(str, "%20", a_space)
		str := """" . str . """" ; in case spaces
		;msgbox % str
		;return
		run % str
		return
	}

	BrowserTab(str)
}

linux()
{
	ifwinexist ahk_exe mintty.exe
	{
		winactivate ahk_exe mintty.exe
		return
	}

	run "C:\projects\linux\WSL Terminal.lnk"
	sleep 300
	winMove, A,, 1272, 38, 1296, 1410
}

ditto() 
{
	send +^!{f11} ; activate ditto
	sleep 50
	ifwinexist ahk_exe Ditto.exe
		winactivate ahk_exe Ditto.exe
	sleep 500
	ifwinexist ahk_exe Ditto.exe
		winactivate ahk_exe Ditto.exe
}

youtubeKey(key)
{
	ifwinnotexist - YouTube - Google Chrome
		return
	WinGet, ActiveId, ID, A
	winactivate - YouTube - Google Chrome

	;send {lwin up}
	;send {rwin up}
	;keywait rwin
	;keywait lwin
	sleep 10
	send {%key%}
	sleep 10
	WinActivate, ahk_id %ActiveId%
}

MyNextRssStory(destar)
{
	global BrowserExe
	IfWinActive IG Dealing Platform
		return
	IfWinActive BitMEX
		return
	IfWinActive Cryptowatch
		return
	;IfWinActive - YouTube
	;	return
	hackerNews := winactive("Hacker News")

	delay = 600
	
	rssInitially := rss_reader_active()
	if (!rssInitially)
	{
		browserCloseTab()
		Sleep delay
	}
		
	;if (!rss_reader_active())
	;{
	;	Send ^1
	;	Sleep delay
	;}
	
	Loop
	{
		if (rss_reader_active())
			break

		ifwinnotactive ahk_exe %BrowserExe%
			return
			
		Send ^{PgUp}
		Sleep delay
	}

	goToComments := !rssInitially && !hackerNews && destar
	if (goToComments)
	{
		send o
		sleep 400
		if (opencomments())
			return
	}

	delay2 = 100
	Send {esc}
	if (destar)
	{
		Sleep delay2
		Send s
	}
	Sleep delay2
	global rssdir
	send m
	if (rssdir == 0)
		Send p
	else
		Send n
	Sleep delay2
	Send v
}

BrowserTab(url)
{
	Browser()
	global BrowserLocation
	global BrowserExe
	If (url == "")
		str := BrowserLocation . " -new-tab about:blank"
	else if (RegExMatch(url, "((mailto\:|(news|(ht|f)tp(s?))\://){1}\S+)"))
		str := BrowserLocation . " -new-tab " . url
	Else
		str := BrowserLocation . " -new-tab ""https://www.google.co.uk/search?q=" . url . """"
	;str := BrowserLocation . " " . url
	;msgbox % str
	keywait alt
	sleep 50
	run %str%
	sleep 50

	If (url == "")
	{
		winwaitactive about:blank
		sleep 150
		send ^l
	}
	;msgbox % BrowserLocation
	;while (1)
	;sleep 300
	;while (winactive("ahk_class MozillaWindowClass") == 0)
	;{
	;	;winwaitactive ahk_class ahk_class Chrome_WidgetWin_1
	;	WinActivate	ahk_class MozillaWindowClass
	;	sleep 300
	;	;SOUNDbeep 400, 50
	;}
}

codeapp()
{
	ifwinexist IntelliJ IDEA
		winactivate IntelliJ IDEA
	else
		visualstudio()
}