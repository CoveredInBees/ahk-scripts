#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#include c:\data\autohotkey\locationdependent_defs.ahk
#include %A_ScriptDir%\util.ahk

str = %A_ScriptDir%
;MyAHK := "C:/Program Files (x86)/AutoHotkey/AutoHotkey.exe"

str := MyAHK . "\AutoHotkey.exe " . str . "/main.ahk"
run % str
;ipython()
;spyder()
startupDialog()

startupDialog()
{
	global pidgin_startup
	if (pidgin_startup)
	{
		SetTimer, MoveMsgBox, 20
		MsgBox, 36, , Would you like to run Pidgin, sir?

		IfMsgBox Yes
			startPidgin()	
	}
}

MoveMsgBox:
	SetTimer, MoveMsgBox, OFF
	WinMove, startup.ahk ahk_class #32770, , 1299, 416
	Return
